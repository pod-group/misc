from cadquery import cq, CQ

d1 = 60
d2 = 15
h1 = 15

c1 = CQ().sketch().circle(d1/2).circle(d2/2, mode='s').finalize().extrude(h1)


d3 = 26
d4 = 15
h2 = 90
c2 = CQ().sketch().circle(d3/2).circle(d4/2, mode='s').finalize().extrude(h2)

w = CQ(c1.findSolid()).union(c2.findSolid())

show_object(w)