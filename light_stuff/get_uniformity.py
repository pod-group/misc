#!/usr/bin/env python3

# colletcs some uniformity data
from xml.dom.domreg import well_known_implementations
import dataset
import serial
import seabreeze.spectrometers as sb
import numpy as np
import matplotlib.pyplot as plt
import pickle
import time
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)

# +++++all blue board+++++
# ===ch 1===
# part: GDCSSRM2.14
# color: blue
# centroid[nm]: 450

# +++++combo board+++++
# ===ch 1===
# part: SFH4716AS
# color: IR
# centroid[nm]: 850
# angle[deg]: 150
# max junction temp[degC]: 145
# max power[W]: 5.8
# max continuous I[mA]: 1500
# max fwd voltage@1.5A[V]: 3.85
# total radiant flux@1000mA[mW]: 1530
# total radiant flux@1500mA[mW]: 2250
# ===ch 2===
# part: GDCSHPM1.14
# color: blue
# dominant wavelength[nm]: 455
# angle[deg]: 150
# max junction temp[degC]: 135
# max continuous I[mA]: 1000
# max fwd voltage@350mA[V]: 3.25
# max fwd voltage@1.5A[V]: 3.85
# total radiant flux@350mA[mW]: 765


loaded = False  # is running from loaded data file
loadfile = "1645452704.8489687.pkl" # 25
loadfile = "1645453071.7434263.pkl" # 35 ~30mW
loadfile = "1645453312.2590692.pkl" # 40 ~ 20mW
loadfile = "1645454141.789455.pkl" # 45  ~16mW @445nm
try:
    with open(loadfile,'rb') as f:
        series, height = pickle.load(f)
    loaded = True
except Exception as e:
    height = 45
    print("Setting up spectrometer")
    spec = sb.Spectrometer.from_serial_number()
    expose_ms = 7.5
    spec.integration_time_micros(expose_ms*1000)
    wls = spec.wavelengths()
    print("Spectrometer ready")

    def fetch_spec():
        return spec.intensities(correct_dark_counts=True, correct_nonlinearity=True)

    # some details related to the firmware
    cmd_term = '\t'
    fw_prompt = '>>> '

    # some details related to the setup
    port = '/dev/ttyUSB0'
    timeout = 90  # in s, no motion (including homing) should take longer than this

    n_points = 100

    # 2370000 steps is about 371 mm
    motion_length_steps = 2370000
    motion_length_mm = 371

    # number of steps to move each time
    spacing_steps = int(motion_length_steps/(n_points-1))


    def stage(s, cmd):
        msg = str(cmd)+cmd_term
        s.write(msg.encode())
        resp = s.read_until(fw_prompt.encode()).decode().rstrip(fw_prompt).strip()
        return (resp)

    point_data = ()
    series = []
    print("Connecting, initializing...")
    with serial.Serial(port, 9600, timeout=timeout) as ser:
        # wait for firmware to come up (appearance of hello message)
        ser.read_until("begin.".encode())
        ser.write('b'.encode())  # send begin signal (hardware inits now)

        # wait for hardware  init to complete and command prompt to appear
        ser.read_until(fw_prompt.encode())
        print("Ready for commands!\n")

        for pos in np.linspace(0, motion_length_mm, n_points):
            if pos != 0:
                stage(ser, spacing_steps)
            print(f"fetch {pos}")
            point_data = (wls, fetch_spec(), pos)
            series.append(point_data)
        print("Homing...")
        stage(ser, 'hr')
        print("Done.")

    spec.close()

    print("Done measuring!")

print("Plotting")

projection = "3d"

ax3d = plt.figure().add_subplot(projection=projection)
ax3d.autoscale(enable=True, tight=True)

projection = None
ax = plt.figure().add_subplot(projection=projection)
ax.autoscale(enable=True, tight=True)

projection = None
axe = plt.figure().add_subplot(projection=projection)
axe.autoscale(enable=True, tight=True)
wl_l = 435
wl_u = 445

projection = None
axe2 = plt.figure().add_subplot(projection=projection)
axe2.autoscale(enable=True, tight=True)

sums = np.array([])
locs = np.array([])
plt.subplots_adjust(bottom=0.2)
for point in series:
    loc = point[2]
    specy = point[1]
    w = point[0]
    l3, = ax3d.plot(w, loc*np.ones(len(w)), specy, label=f"{loc:.2f}mm")
    l, = ax.plot(w, specy, label=f"{loc:.2f}mm")
    irng = (w<wl_u) & (w>wl_l)
    sums = np.append(sums, np.sum(specy[irng]))
    locs = np.append(locs, loc)

ax3d.set(xlabel='Wavelength [nm]', ylabel='Position [mm]', zlabel='Intensity [counts]')
ax.set_title(f"At {height}mm from the array")
#ax3d.legend()
ax3d.grid(which='both')

ax.set(xlabel='Wavelength [nm]', ylabel='Intensity [counts]')
ax.set_title(f"At {height}mm from the array")
#ax.legend()
ax.grid(which='both')

le, = axe.plot(locs, sums, '-o')
axe.set(xlabel='Position [mm]', ylabel=f"Total counts between {wl_l} and {wl_u}nm")
axe.set_title(f"At {height}mm from the array")
axe.grid(which='both')

le, = axe2.plot(locs, sums/(np.max(sums)), '-o')
axe2.set(xlabel='Position [mm]', ylabel=f"Total counts between {wl_l} and {wl_u}nm (scaled)")
axe2.set_title(f"At {height}mm from the array")
axe2.grid(which='both')
axe2.yaxis.set_major_locator(MultipleLocator(0.05))

# Turn grid on for both major and minor ticks and style minor slightly
# differently.
axe2.grid(which='major', color='#CCCCCC', linestyle='--')
axe2.grid(which='minor', color='#CCCCCC', linestyle=':')


if loaded == False:
    with open(f"{time.time()}.pkl",'wb') as f:
        pickle.dump((series, height), f)

plt.show()
