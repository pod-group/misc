# notes
- want input current = 1A so that heat gen = R, then 1/(10e-3\*10e-3) = 1e4 A/m^2 if we're injecting via a 1sqcm surface
- metal= silver, ag conductivity = 6.30e7 S/m
- izo conductivity = 2.0e5 S/m (from J. Perkins, NREL), thickness = 60nm, then Rs = 83.3 ohm/sq

# results
- simulation takes ~20 sec.
- effective R is 2.209 ohm, then power loss at 20mA/cm^2 is 0.883 mW (aka 0.883mw/cm^2, aka reduction of 0.883 PCE points)
