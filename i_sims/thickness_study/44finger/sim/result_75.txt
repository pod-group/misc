ELMER SOLVER (v 9.0) STARTED AT: 2023/12/11 01:09:30
ParCommInit:  Initialize #PEs:            1
MAIN: 
MAIN: =============================================================
MAIN: ElmerSolver finite element software, Welcome!
MAIN: This program is free software licensed under (L)GPL
MAIN: Copyright 1st April 1995 - , CSC - IT Center for Science Ltd.
MAIN: Webpage http://www.csc.fi/elmer, Email elmeradm@csc.fi
MAIN: Version: 9.0 (Rev: 376bbd1, Compiled: 2023-11-26)
MAIN:  Running one task without MPI parallelization.
MAIN:  Running with just one thread per task.
MAIN:  MUMPS library linked in.
MAIN:  Lua interpreted linked in.
MAIN: =============================================================
LoadInputFile: Reading only "Run Control" section
MAIN: 
MAIN: 
MAIN: -------------------------------------
MAIN: Reading Model: case.sif
LoadInputFile: Scanning input file: case.sif
LoadInputFile: Scanning only size info
LoadInputFile: First time visiting
LoadInputFile: Reading base load of sif file
LoadInputFile: Loading input file: case.sif
LoadInputFile: Reading base load of sif file
LoadInputFile: Number of BCs: 2
LoadInputFile: Number of Body Forces: 0
LoadInputFile: Number of Initial Conditions: 0
LoadInputFile: Number of Materials: 3
LoadInputFile: Number of Equations: 1
LoadInputFile: Number of Solvers: 1
LoadInputFile: Number of Bodies: 2
WARNING:: LoadInputFile: > Material 3 < not used in any Body!
ElmerAsciiMesh: Base mesh name: ./.
MapCoordinates: Scaling coordinates: 1.000E-09 1.000E-09 1.000E-09
LoadMesh: Elapsed REAL time:     3.6075 (s)
MAIN: -------------------------------------
AddVtuOutputSolverHack: Adding ResultOutputSolver to write VTU output in file: case
OptimizeBandwidth: ---------------------------------------------------------
OptimizeBandwidth: Computing matrix structure for: static current conduction...done.
OptimizeBandwidth: Half bandwidth without optimization: 1409222
OptimizeBandwidth: 
OptimizeBandwidth: Bandwidth Optimization ...done.
OptimizeBandwidth: Half bandwidth after optimization: 19887
OptimizeBandwidth: ---------------------------------------------------------
ElmerSolver: Number of timesteps to be saved: 1
MAIN: 
MAIN: -------------------------------------
MAIN:  Steady state iteration:            1
MAIN: -------------------------------------
MAIN: 
StatCurrentSolve: -------------------------------------
StatCurrentSolve: STAT CURRENT SOLVER:
StatCurrentSolve: -------------------------------------
StatCurrentSolve: Static current iteration: 1
StatCurrentSolve:    Assembly:   5 % done
StatCurrentSolve:    Assembly:  12 % done
StatCurrentSolve:    Assembly:  18 % done
StatCurrentSolve:    Assembly:  24 % done
StatCurrentSolve:    Assembly:  30 % done
StatCurrentSolve:    Assembly:  36 % done
StatCurrentSolve:    Assembly:  42 % done
StatCurrentSolve:    Assembly:  48 % done
StatCurrentSolve:    Assembly:  55 % done
StatCurrentSolve:    Assembly:  61 % done
StatCurrentSolve:    Assembly:  67 % done
StatCurrentSolve:    Assembly:  73 % done
StatCurrentSolve:    Assembly:  79 % done
StatCurrentSolve:    Assembly:  85 % done
StatCurrentSolve:    Assembly:  91 % done
StatCurrentSolve:    Assembly:  98 % done
StatCurrentSolve:  Assembly (s)          :   16.520313999999999
ComputeChange: NS (ITER=1) (NRM,RELC): ( 0.18241259      2.0000000     ) :: static current conduction
StatCurrentSolve:  Solve (s)             :   3021.7720920000002
StatCurrentSolve:  Total Heating Power   :   1.3032295952272181E-002
StatCurrentSolve: Static current iteration: 2
StatCurrentSolve:    Assembly:   5 % done
StatCurrentSolve:    Assembly:  12 % done
StatCurrentSolve:    Assembly:  18 % done
StatCurrentSolve:    Assembly:  24 % done
StatCurrentSolve:    Assembly:  30 % done
StatCurrentSolve:    Assembly:  36 % done
StatCurrentSolve:    Assembly:  42 % done
StatCurrentSolve:    Assembly:  48 % done
StatCurrentSolve:    Assembly:  55 % done
StatCurrentSolve:    Assembly:  61 % done
StatCurrentSolve:    Assembly:  67 % done
StatCurrentSolve:    Assembly:  73 % done
StatCurrentSolve:    Assembly:  79 % done
StatCurrentSolve:    Assembly:  85 % done
StatCurrentSolve:    Assembly:  91 % done
StatCurrentSolve:    Assembly:  98 % done
StatCurrentSolve:  Assembly (s)          :   16.572684000000208
ComputeChange: NS (ITER=2) (NRM,RELC): ( 0.18241259      0.0000000     ) :: static current conduction
StatCurrentSolve:  Solve (s)             :   2995.4502839999996
StatCurrentSolve:  Total Heating Power   :   1.3032295952272181E-002
ComputeChange: SS (ITER=1) (NRM,RELC): ( 0.18241259      2.0000000     ) :: static current conduction
ResultOutputSolver: -------------------------------------
ResultOutputSolver: Saving with prefix: case
ResultOutputSolver: Creating list for saving - if not present
CreateListForSaving: Field Variables for Saving
ResultOutputSolver: Saving in unstructured VTK XML (.vtu) format
VtuOutputSolver: Saving results in VTK XML format with prefix: case
VtuOutputSolver: Saving number of partitions: 1
ResultOutputSolver: -------------------------------------
ElmerSolver: *** Elmer Solver: ALL DONE ***
ElmerSolver: The end
SOLVER TOTAL TIME(CPU,REAL):      6082.79      344.41
ELMER SOLVER FINISHED AT: 2023/12/11 01:15:15
