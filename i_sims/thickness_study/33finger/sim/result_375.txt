ELMER SOLVER (v 9.0) STARTED AT: 2023/12/11 08:16:21
ParCommInit:  Initialize #PEs:            1
MAIN: 
MAIN: =============================================================
MAIN: ElmerSolver finite element software, Welcome!
MAIN: This program is free software licensed under (L)GPL
MAIN: Copyright 1st April 1995 - , CSC - IT Center for Science Ltd.
MAIN: Webpage http://www.csc.fi/elmer, Email elmeradm@csc.fi
MAIN: Version: 9.0 (Rev: 376bbd1, Compiled: 2023-11-26)
MAIN:  Running one task without MPI parallelization.
MAIN:  Running with just one thread per task.
MAIN:  MUMPS library linked in.
MAIN:  Lua interpreted linked in.
MAIN: =============================================================
LoadInputFile: Reading only "Run Control" section
MAIN: 
MAIN: 
MAIN: -------------------------------------
MAIN: Reading Model: case.sif
LoadInputFile: Scanning input file: case.sif
LoadInputFile: Scanning only size info
LoadInputFile: First time visiting
LoadInputFile: Reading base load of sif file
LoadInputFile: Loading input file: case.sif
LoadInputFile: Reading base load of sif file
LoadInputFile: Number of BCs: 2
LoadInputFile: Number of Body Forces: 0
LoadInputFile: Number of Initial Conditions: 0
LoadInputFile: Number of Materials: 3
LoadInputFile: Number of Equations: 1
LoadInputFile: Number of Solvers: 1
LoadInputFile: Number of Bodies: 2
WARNING:: LoadInputFile: > Material 3 < not used in any Body!
ElmerAsciiMesh: Base mesh name: ./.
MapCoordinates: Scaling coordinates: 1.000E-09 1.000E-09 1.000E-09
LoadMesh: Elapsed REAL time:     2.4850 (s)
MAIN: -------------------------------------
AddVtuOutputSolverHack: Adding ResultOutputSolver to write VTU output in file: case
OptimizeBandwidth: ---------------------------------------------------------
OptimizeBandwidth: Computing matrix structure for: static current conduction...done.
OptimizeBandwidth: Half bandwidth without optimization: 951896
OptimizeBandwidth: 
OptimizeBandwidth: Bandwidth Optimization ...done.
OptimizeBandwidth: Half bandwidth after optimization: 13651
OptimizeBandwidth: ---------------------------------------------------------
ElmerSolver: Number of timesteps to be saved: 1
MAIN: 
MAIN: -------------------------------------
MAIN:  Steady state iteration:            1
MAIN: -------------------------------------
MAIN: 
StatCurrentSolve: -------------------------------------
StatCurrentSolve: STAT CURRENT SOLVER:
StatCurrentSolve: -------------------------------------
StatCurrentSolve: Static current iteration: 1
StatCurrentSolve:    Assembly:   8 % done
StatCurrentSolve:    Assembly:  17 % done
StatCurrentSolve:    Assembly:  26 % done
StatCurrentSolve:    Assembly:  36 % done
StatCurrentSolve:    Assembly:  45 % done
StatCurrentSolve:    Assembly:  54 % done
StatCurrentSolve:    Assembly:  63 % done
StatCurrentSolve:    Assembly:  72 % done
StatCurrentSolve:    Assembly:  81 % done
StatCurrentSolve:    Assembly:  90 % done
StatCurrentSolve:  Assembly (s)          :   11.105560000000001
ComputeChange: NS (ITER=1) (NRM,RELC): ( 0.83738302E-01  2.0000000     ) :: static current conduction
StatCurrentSolve:  Solve (s)             :   1928.4883840000000
StatCurrentSolve:  Total Heating Power   :   6.1316862805527277E-003
StatCurrentSolve: Static current iteration: 2
StatCurrentSolve:    Assembly:   8 % done
StatCurrentSolve:    Assembly:  17 % done
StatCurrentSolve:    Assembly:  27 % done
StatCurrentSolve:    Assembly:  36 % done
StatCurrentSolve:    Assembly:  45 % done
StatCurrentSolve:    Assembly:  54 % done
StatCurrentSolve:    Assembly:  63 % done
StatCurrentSolve:    Assembly:  72 % done
StatCurrentSolve:    Assembly:  81 % done
StatCurrentSolve:    Assembly:  90 % done
StatCurrentSolve:    Assembly:  99 % done
StatCurrentSolve:  Assembly (s)          :   11.171384000000216
ComputeChange: NS (ITER=2) (NRM,RELC): ( 0.83738302E-01  0.0000000     ) :: static current conduction
StatCurrentSolve:  Solve (s)             :   1913.7323680000002
StatCurrentSolve:  Total Heating Power   :   6.1316862805527277E-003
ComputeChange: SS (ITER=1) (NRM,RELC): ( 0.83738302E-01  2.0000000     ) :: static current conduction
ResultOutputSolver: -------------------------------------
ResultOutputSolver: Saving with prefix: case
ResultOutputSolver: Creating list for saving - if not present
CreateListForSaving: Field Variables for Saving
ResultOutputSolver: Saving in unstructured VTK XML (.vtu) format
VtuOutputSolver: Saving results in VTK XML format with prefix: case
VtuOutputSolver: Saving number of partitions: 1
ResultOutputSolver: -------------------------------------
ElmerSolver: *** Elmer Solver: ALL DONE ***
ElmerSolver: The end
SOLVER TOTAL TIME(CPU,REAL):      3886.28      219.43
ELMER SOLVER FINISHED AT: 2023/12/11 08:20:01
