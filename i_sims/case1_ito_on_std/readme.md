# notes
- want input current = 1A so that heat gen = R, then 1/(10e-3\*10e-3) = 1e4 A/m^2 if we're injecting via a 1sqcm surface
- metal= silver, ag conductivity = 6.30e7 S/m
- ito conductivity = 4.0e5 S/m, thickness = 145nm, then Rs=17.24

# results
- simulation takes ~20 sec.
- effective R is 1.68 ohm, then power loss at 20mA/cm^2 is 0.672mW is 0.672mW (aka 0.672mw/cm^2, aka reduction of 0.672PCE points)
