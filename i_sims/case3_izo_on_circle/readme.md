# notes
- want input current = 1A so that heat gen = R, then 1/(10e-3\*10e-3) = 1e4 A/m^2 if we're injecting via a 1sqcm surface
- metal= silver, ag conductivity = 6.30e7 S/m
- izo conductivity = 2.0e5 S/m, thickness = 60nm, then Rs = 83.3 ohm/sq

# results
- simulation takes ~20 sec.
- effective R is 5.31 ohm, then power loss at 20mA/cm^2 is 2.12mW (aka 2.12 mw/cm^2, aka reduction of 2.12 PCE points)
