metal_t = 100;
tco_t = 60;
tco_layers = 10;

If(metal_t > 250)
	metal_nm_per_layer = 25;
ElseIf(metal_t > 50)
	metal_nm_per_layer = 10;
Else
	metal_nm_per_layer = 5;
EndIf

SetFactory("OpenCASCADE");
Merge "tstudy.step";
Coherence;

Mesh.MeshSizeMax = 2e5;  // for nm (default)
Mesh.MeshSizeExtendFromBoundary = 1;
Mesh.MeshSizeFromCurvature = 100;
Mesh.MeshSizeFromCurvatureIsotropic = 1;
Mesh.FlexibleTransfinite = 1;
Mesh.Algorithm = 6;
Mesh.RecombineAll = 1;
Mesh.RecombinationAlgorithm = 3;
Mesh.Algorithm3D = 1;
Mesh.Recombine3DAll = 1;
Mesh.Recombine3DLevel = 2;
Mesh.Recombine3DConformity = 4;

//z_scale = 1e4;  // good for inspection/setup
z_scale = 1; // layers in nm (default)

//Abort;
Mesh 2;
RecombineMesh;
//Abort;

Extrude {0, 0, -tco_t*z_scale} {
  Surface{:}; Layers {tco_layers}; Recombine;
}

metal_layers = Round(metal_t/metal_nm_per_layer);
Extrude {0, 0, metal_t*z_scale} {
  Surface{2, 4, 6, 7, 10, 11, 14, 15, 18, 19, 22}; Layers {metal_layers}; Recombine;
}

Mesh 3;
Coherence Mesh;
//RecombineMesh;  // this appears to kill the gen surface
OptimizeMesh "Gmsh";

//Abort;
//Physical Surface("gen", 247) = {185, 190, 200, 205, 220, 225, 240, 245, 260, 265, 286};
Physical Surface("gen", 247) = {154};
//Physical Surface("gnd", 248) = {70, 287};
Physical Surface("gnd", 248) = {70, 174};
Physical Volume("tco", 249) = {1:22};
Physical Volume("metal", 250) = {23:33};
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
Save "tstudy.msh";
