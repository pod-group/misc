t_metal = 100;  // [nm]
t_tco = 60;  // [nm]
nm_per_layer = 10;

SetFactory("OpenCASCADE");
Merge "sim_onesqcm_square.step";
Coherance;

Mesh.MeshSizeMax = 1e5;  // for nm (default)
Mesh.Algorithm = 5;
Mesh.RecombineAll = 1;
Mesh.RecombinationAlgorithm = 3;
Mesh.Algorithm3D = 1;
Mesh.Recombine3DAll = 1;
Mesh.Recombine3DLevel = 1;
Mesh.Recombine3DConformity = 2;

//z_scale = 1e4;  // good for inspection/setup
z_scale = 1; // layers in nm (default)

Mesh 2;
RecombineMesh;

this_layers = Round(t_metal/nm_per_layer);
Extrude {0, 0, t_metal*z_scale} {
  Surface{1}; Layers {this_layers}; Recombine;
}

this_layers = Round(t_tco/nm_per_layer);
Extrude {0, 0, -t_tco*z_scale} {
  Surface{1}; Layers {this_layers}; Recombine;
}
Extrude {0, 0, -t_tco*z_scale} {
  Surface{2}; Layers {this_layers}; Recombine;
}
Extrude {0, 0, -t_tco*z_scale} {
  Surface{3}; Layers {this_layers}; Recombine;
}

Mesh 3;
Coherence Mesh;
//RecombineMesh;  // not working here. seems fine...
OptimizeMesh "Gmsh";

//Abort;
Physical Surface("gen", 247) = {99};
Physical Surface("gnd", 248) = {32, 63};
Physical Volume("metal", 249) = {1};
Physical Volume("tco", 250) = {3, 4, 2};
