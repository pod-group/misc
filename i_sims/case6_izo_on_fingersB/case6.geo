t_metal = 100;  // [nm]
t_tco = 60;  // [nm]
nm_per_layer = 10;

SetFactory("OpenCASCADE");
Merge "sim_onesqcm_fingerB.step";
Coherance;

Mesh.MeshSizeMax = 1e5;  // for nm (default)
Mesh.Algorithm = 5;
Mesh.RecombineAll = 1;
Mesh.RecombinationAlgorithm = 3;
Mesh.Algorithm3D = 1;
Mesh.Recombine3DAll = 1;
Mesh.Recombine3DLevel = 1;
Mesh.Recombine3DConformity = 2;

//z_scale = 1e4;  // good for inspection/setup
z_scale = 1; // layers in nm (default)

//Abort;
Mesh 2;
RecombineMesh;

this_layers = Round(t_tco/nm_per_layer);
Extrude {0, 0, -t_tco*z_scale} {
  Surface{:}; Layers {this_layers}; Recombine;
}

this_layers = Round(t_metal/nm_per_layer);
Extrude {0, 0, t_metal*z_scale} {
  Surface{1,3,5,8}; Layers {this_layers}; Recombine;
}

Mesh 3;
Coherence Mesh;
//RecombineMesh;  // not working here. seems fine...
OptimizeMesh "Gmsh";

//Abort;
Physical Surface("gen", 247) = {114, 93, 119, 138};
Physical Surface("gnd", 248) = {15, 145};
Physical Volume("tco", 249) = {1:8};
Physical Volume("metal", 250) = {9:12};
