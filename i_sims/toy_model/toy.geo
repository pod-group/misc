thickness = 135; // thickness of tco [nm]
xy = 30; // tco square xy xims [mm]
stripes_width = 4; // width of metal stripes on both sides
z_layers = 20;
//z_scale = 1e4;  // use a high number here for inspection/setup
z_scale = 1; // everyting in nm (default)

DefineConstant[
  edges = {0, Choices{0="No", 1="Side", 2="Top"},
    Name "Parameters/0Make Metal Edges"}
];

// the geometry will be generated in nm, to be scaled down to meters by the engine
// make the 2d mesh
SetFactory("OpenCASCADE");

Rectangle(1) = {-xy/2*1e6, -xy/2*1e6, 0, xy*1e6, xy*1e6, 0};

If (edges == 1 || edges == 2)
  Rectangle(2) = {-(xy*1e6/2+stripes_width*1e6), -xy/2*1e6, 0, (xy+2*stripes_width)*1e6, xy*1e6, 0};
  BooleanFragments{ Surface{2}; Delete; }{ Surface{1}; Delete; } 
EndIf

Mesh.MeshSizeMax = 5e5;  // for nm (default)
Mesh.Algorithm = 5;  // 5 is swirly, 8 is gridlike
Mesh.RecombineAll = 1;
Mesh.RecombinationAlgorithm = 3;
Mesh.Algorithm3D = 1;
Mesh.Recombine3DAll = 1;
Mesh.Recombine3DLevel = 1;
Mesh.Recombine3DConformity = 2;

Coherance;
Mesh 2;
RecombineMesh;

If (edges == 2)
  Extrude {0, 0, -thickness*z_scale} {
    Surface{:}; Layers {z_layers}; Recombine;
  }
  Extrude {0, 0, thickness*z_scale} {
    Surface{2,3}; Layers {z_layers}; Recombine;
  }
Else
  Extrude {0, 0, thickness*z_scale} {
    Surface{:}; Layers {z_layers}; Recombine;
  }
EndIf

Mesh 3;
Coherence Mesh;
RecombineMesh;  // this step is often difficult and may be skipped
OptimizeMesh "Gmsh";  // probaby not needed

If (edges == 0)
  Physical Volume("tco", 41) = {1};
  Physical Surface("tco_side_in", 42) = {3};
  Physical Surface("tco_side_out", 43) = {5};
  Physical Surface("tco_bottom", 44) = {1};
  Physical Surface("tco_top", 45) = {6};
ElseIf (edges == 1)
  Physical Volume("tco", 41) = {1};
  Physical Surface("tco_side_in", 42) = {5};
  Physical Surface("tco_side_out", 43) = {7};
  Physical Surface("tco_bottom", 44) = {1};
  Physical Surface("tco_top", 45) = {8};
  Physical Volume("metal", 46) = {2, 3};
  Physical Surface("metal_side_in", 47) = {14};
  Physical Surface("metal_side_out", 48) = {9};
  Physical Surface("metal_bottom_in", 49) = {3};
  Physical Surface("metal_bottom_out", 50) = {2};
Else
  Physical Volume("tco", 41) = {1,2,3};
  Physical Surface("tco_side_in", 42) = {5};
  Physical Surface("tco_side_out", 43) = {7};
  Physical Surface("tco_bottom", 44) = {1};
  Physical Surface("tco_top", 45) = {8};
  Physical Volume("metal", 46) = {4, 5};
  Physical Surface("metal_side_in", 47) = {24};
  Physical Surface("metal_side_out", 48) = {17};
  Physical Surface("metal_top_in", 49) = {26};
  Physical Surface("metal_top_out", 50) = {21};
EndIf

// injection current density calcs
// area might be xy[mm]*thickness[nm] = 30e-3*135e-9 = 4.05e-9, 1/4.05e-9 = 2.4691358e8A/m^2, or 2x that = 4.938271e8
// or 30e-3*4e-3 = 4.05e-9 = 1.2e-4, 1/1.2e-4 = 8.33e3

