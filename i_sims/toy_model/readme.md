1. run `gmsh *.geo` and make the gui selection to "make metal edges" or not
1. choose Modules-->Mesh-->Save to export a .msh file
1. run `rungrid.sh`
1. `cd sim && ElmerGUI`
1. load the mesh by opening the sim/ folder (this reads in all the mesh.* files)
1. use sim/case.sif as a reference to set up the elmer simulation:
1. set the geometry scaling to 1e-9
1. set the equation to Static Current Conduction, enable volume current & heating calcs
1. set the linear solver to Direct, Umfpack
1. set ito conductivity to 4.00e5 (hint: a 135nm film of this has Rs=18.51547ohm/sq) or izo to 2.00e5
1. set gold conductivity to 4.11e7 or silver to 6.30e7
1. set the ground (0V) and current injection boundary conditions (2.469e8A/m if injecting from a thin edge) and assign them to appropriate surfaces
1. run the simulation. it should be a few seconds, converge in 1-2 rounds and show 3.037e-16 W of heat generated if you injected into a 135nm tall edge
1. then R=Rs can be calcualted by P/I^2=3.037e-16/(30e-3\*135e-9)^2
1. inspect the result sim/\*.vtu file with paraview
