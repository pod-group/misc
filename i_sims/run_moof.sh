#!/usr/bin/env bash
# needs perl for math

set -o pipefail
set -o errexit
set -o nounset

SECONDS=0

STUDY_FOLDER="moof_study"

rm -rf "${STUDY_FOLDER}"
mkdir -p "${STUDY_FOLDER}"/{moof,moof2}/sim

cp case5_izo_on_fingersA/case5.geo "${STUDY_FOLDER}"/moof/tstudy.geo

sed 's,sim_onesqcm_fingerA.step,tstudy.step,' -i "${STUDY_FOLDER}"/moof/tstudy.geo

cp case5_izo_on_fingersA/sim_onesqcm_fingerA.step "${STUDY_FOLDER}"/moof/tstudy.step

cp case5_izo_on_fingersA/sim/{case.sif,ELMERSOLVER_STARTINFO} "${STUDY_FOLDER}"/moof/sim/.

#sed 's,^Solver 1,Solver 1\n  Linear System Precondition Recompute = 0\n  Linear System Max Iterations = 1,' -i "${STUDY_FOLDER}"/33finger/sim/case.sif
#sed 's,^Solver 1,Solver 1\n  Linear System Precondition Recompute = 0\n  Linear System Max Iterations = 1,' -i "${STUDY_FOLDER}"/44finger/sim/case.sif

# these changes disable current generation under the fingers
#sed 's|^Physical Surface("gen", 247) = .*|Physical Surface("gen", 247) = {104};|' -i "${STUDY_FOLDER}"/23finger/tstudy.geo
#sed 's|^Physical Surface("gen", 247) = .*|Physical Surface("gen", 247) = {114};|' -i "${STUDY_FOLDER}"/33finger/tstudy.geo
#sed 's|^Physical Surface("gen", 247) = .*|Physical Surface("gen", 247) = {124};|' -i "${STUDY_FOLDER}"/34finger/tstudy.geo
#sed 's|^Physical Surface("gen", 247) = .*|Physical Surface("gen", 247) = {134};|' -i "${STUDY_FOLDER}"/44finger/tstudy.geo
#sed 's|^Physical Surface("gen", 247) = .*|Physical Surface("gen", 247) = {144};|' -i "${STUDY_FOLDER}"/45finger/tstudy.geo
#sed 's|^Physical Surface("gen", 247) = .*|Physical Surface("gen", 247) = {154};|' -i "${STUDY_FOLDER}"/55finger/tstudy.geo
# the light aperature is 440.9866 mm^2 and one finger blocks 1.7157 mm^2
# so  the 23 finger mask blocks 1.7157*5 =  8.5785 mm^2 (1.9453% for an assumed PCE loss of 30*0.019453=0.58359 points off)
# and the 33 finger mask blocks 1.7157*6 = 10.2942 mm^2 (2.3344% for an assumed PCE loss of 30*0.023344=0.70031 points off)
# and the 34 finger mask blocks 1.7157*7 = 12.0099 mm^2 (2.7234% for an assumed PCE loss of 30*0.027234=0.81702 points off)
# and the 44 finger mask blocks 1.7157*8 = 13.7256 mm^2 (3.1124% for an assumed PCE loss of 30*0.031124=0.93374 points off)
# and the 45 finger mask blocks 1.7157*9 = 15.4413 mm^2 (3.5015% for an assumed PCE loss of 30*0.035015=1.05046 points off)
# and the 45 finger mask blocks 1.7157*10 =17.157  mm^2 (3.8906% for an assumed PCE loss of 30*0.035015=1.16718 points off)


METAL_THICKNESSES=(
#25
#50
#75
100
#125
#150
#175
#200
#225
#250
#275
#300
#325
#350
#375
#400
#425
#450
#475
#500
)

# target TCO sheet resistance [ohm/sq]
RSS=(
5
10
15
20
30
40
50
75
83.3333333333333
100
125
150
200
)

# tco thickness [nm]
TCO_T=60

#VAR_NAME="Metal Thickness [nm]"
VAR_NAME="TCO sheet resistance [ohm/sq]"
echo -e "${VAR_NAME}\tP23 [W]\tP33 [W]\tP34 [W]\tP44 [W]\tP45 [W]\tP55 [W]" > "${STUDY_FOLDER}"/results.tsv

for rs in "${RSS[@]}"
do
	for t in "${METAL_THICKNESSES[@]}"
	do
		#RSLT_LINE="${t}"
		RSLT_LINE="${rs}"
		for FOLDER in moof
		do
			cd "${STUDY_FOLDER}"/${FOLDER}
			sed "s,^metal_t = .*,metal_t = ${t};," -i tstudy.geo
			sed "s,^tco_t = .*,tco_t = ${TCO_T};," -i tstudy.geo
			echo 'Save "tstudy.msh";' >> tstudy.geo
			gmsh tstudy.geo -parse_and_exit || true
			echo "wat2"
			../../rungrid.sh

			cd -
			echo "what"
			cd "${STUDY_FOLDER}"/${FOLDER}/sim
			sed 's,Current Density = .*,Current Density = 200,' -i case.sif
			CONDUCTIVITY=$(echo -e "1/${rs}*1/${TCO_T}e-9" | perl -nle 'print eval')
			sed "/Name = \"izo\"/,/^End/ s/Electric Conductivity = .*/Electric Conductivity = ${CONDUCTIVITY}/" -i case.sif
			echo "running ${FOLDER} t=${t}nm, rs=${rs}S/m..."
			ElmerSolver | tee result_${t}_${rs}.txt
			cp case_t0001.vtu metal_t${t}_${rs}.vtu
			P=$(grep -m 1 "Total Heating Power" result_${t}_${rs}.txt | awk '{print $6}')
			RSLT_LINE="${RSLT_LINE}\t${P}"
			echo "Line result:"
			echo -e "${RSLT_LINE}"
			cd -
		done

		echo -e "${RSLT_LINE}" >> "${STUDY_FOLDER}"/results.tsv
		echo "Cumulitive result:"
		cat "${STUDY_FOLDER}"/results.tsv
	done
done

echo "Final result:"
cat "${STUDY_FOLDER}"/results.tsv

echo "Elapsed: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"
