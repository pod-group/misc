# notes
- if J=0.02mA/cm^2 then I = 0.0882A
- want input current = 1A so that heat gen = R, then 1/(440.9866e-6) = 2267.64259957 A/m^2 if we're injecting via a 4.41sqcm surface
- metal= silver, ag conductivity = 6.30e7 S/m
- izo conductivity = 2.0e5 S/m (from J. Perkins, NREL), thickness = 60nm, then Rs = 83.3 ohm/sq

# results
- simulation takes ~20 sec.
- effective R is 1.62 ohm, then power loss at 20mA/cm^2 is (4.41\*0.02)^2\*1.62 = 12.6mW (aka 2.86 mW/cm^2, aka reduction of 2.86 PCE points)
