#!/usr/bin/env python3

import cadquery
from cadquery import CQ
import geometrics.toolbox.utilities as tbutil
from geometrics.toolbox import twod_to_threed
from cq_warehouse.fastener import CounterSunkScrew
import cq_warehouse.extensions  # this does something even though it's not directly used
import pathlib
import logging
import math


class LaserLabelHolder(object):
    # units are mm
    x_nom = 30.0  # nominal substrate x dim
    y_nom = 30.0  # nominal substrate y dim
    xy_extra = 0.2  # add this to substrate x&y dims to find pocket size
    shelf_height = 10  # to raise the lower substrate surface this much off the support base (0.5 for C02 laser tray)
    wall_height = 3.3  # height of wall between substrates (def 0.75 for laser tray)
    support_plate_t = 10  # strengthening underplate
    nx = 5  # number of slots in x
    ny = 5  # number of slots in y

    # integers for shifting where the center void is
    cv_shift_x = 0
    cv_shift_y = 1

    thread_depth = shelf_height + wall_height + support_plate_t

    #thread_diameter_major = 6  # outer diameter for threads i.e. 6 for M6
    taphole_diameter = 5
    #x_wall = 2*2**0.5/2 * thread_diameter_major/2
    #y_wall = 2*2**0.5/2 * thread_diameter_major/2

    #taphole_diameter = 0
    x_space = 390  # available x dimension
    y_space = 300  # available y dimension

    cut_tool_diameter = 10  # assume a round cutting tool with this diameter
    cut_tool_diameter_big = 20  # assume a round cutting tool with this diameter for the center cutout

    tweezer_allowance_depth = 0.5  # tweezer wells should go this far below the bottom of the substrate
    tweezer_allowance_width = 8  # width of tweezer slots

    hole_d = 20  # hole in underplate

    max_substrate_thickness = 2.2
    acceptance_angle = 45  # 90 here would be for perfectly collimated light
    pin_side_pixel_edge_spacing = 8.34
    non_pin_side_pixel_edge_spacing = 4.25
    hole_offset_from_pixel_edge = max_substrate_thickness/math.tan(math.radians(acceptance_angle))
    lid_hole = (x_nom+xy_extra-(pin_side_pixel_edge_spacing-hole_offset_from_pixel_edge)*2, y_nom+xy_extra-(non_pin_side_pixel_edge_spacing-hole_offset_from_pixel_edge)*2)

    lid_fitment = 0.2
    lid_inner_edge_height = 0.2
    after_chamfer_edge_gap = 0.1
    lid_hole_fillet = 5

    chamfer = 0.5

    def __init__(self, nx=5, ny=5):
        # setup logging
        self.lg = logging.getLogger(__name__)
        self.lg.setLevel(logging.DEBUG)
        self.nx = nx
        self.ny = ny

        self.x_wall = (self.x_space - self.nx * (self.x_nom + self.xy_extra))/self.nx
        self.y_wall = (self.y_space - self.ny * (self.y_nom + self.xy_extra))/self.ny

        self.unit_x = self.x_nom + self.xy_extra + self.x_wall
        self.unit_y = self.y_nom + self.xy_extra + self.y_wall

        min_lid_gap = min(self.unit_x - self.lid_hole[0], self.unit_y - self.lid_hole[1])
        self.lid_chamfer_thickness = (min_lid_gap/2 - self.lid_fitment/2 - self.after_chamfer_edge_gap)*math.tan(math.radians(self.acceptance_angle))
        #self.lid_chamfer_thickness = (min_lid_gap/2 - self.lid_fitment/2 - self.after_chamfer_edge_gap)*math.tan(math.radians(self.acceptance_angle))
        self.lid_thickness = self.lid_chamfer_thickness + self.lid_inner_edge_height

        ch = logging.StreamHandler()
        logFormat = logging.Formatter(("%(asctime)s|%(name)s|%(levelname)s|%(message)s"))
        ch.setFormatter(logFormat)
        self.lg.addHandler(ch)

    def make_thing(self, cvx=0, cvy=0):
        s = self
        fudge = 0

        x_len = s.nx * s.unit_x
        y_len = s.ny * s.unit_y
        z_len = s.shelf_height + s.wall_height

        CQ.undercutRelief2D = tbutil.undercutRelief2D
        one_void = CQ().box(s.x_nom + s.xy_extra, s.y_nom + s.xy_extra, z_len, centered=(True, True, False)).edges("|Z").fillet(s.cut_tool_diameter / 2)  # under substrate voids
        tweezer_void_x = CQ().box(s.unit_x + fudge, s.tweezer_allowance_width, s.wall_height + s.tweezer_allowance_depth, centered=(True, True, False))
        tweezer_void_y = CQ().box(s.tweezer_allowance_width, s.unit_y + fudge,  s.wall_height + s.tweezer_allowance_depth, centered=(True, True, False))
        tweezer_void = tweezer_void_x.union(tweezer_void_y)
        fatten_up = CQ().box(x_len, y_len, s.support_plate_t, centered=(True, True, False)).rarray(s.unit_x, s.unit_y, s.nx, s.ny, center=True).circle(s.hole_d / 2).cutThruAll().faces(">Z").edges("not %Line").chamfer(s.chamfer)
        if s.taphole_diameter and s.thread_depth:
            thread_void = CQ().cylinder(height=s.thread_depth, radius=s.taphole_diameter/2, centered= (True, True, False))
        else:
            thread_void = None

        s.center_void_xshift = s.unit_x*s.cv_shift_x
        if (s.nx+cvx)%2:
            s.center_void_xshift += s.unit_x/2

        s.center_void_yshift = s.unit_y*s.cv_shift_y
        if (s.ny+cvy)%2:
            s.center_void_yshift += s.unit_y/2

        h00 = CQ().box(x_len, y_len, z_len, centered=(True, True, False))  # limits box

        h01 = h00.faces(">Z").workplane().rarray(s.unit_x, s.unit_y, s.nx, s.ny, center=True).rect(s.y_nom + s.xy_extra, s.y_nom + s.xy_extra).cutBlind(-s.wall_height)  # substrate pockets
        #h01 = h00.faces(">Z").workplane().rarray(x_len / nx, y_len / ny, nx, ny, center=True).undercutRelief2D(s.y_nom + s.xy_extra, s.y_nom + s.xy_extra, diameter=s.cut_tool_diameter).cutBlind(-s.wall_height)  # substrate pockets

        all_voids = h00.faces("<Z").workplane(invert=True).rarray(s.unit_x, s.unit_y, s.nx, s.ny, center=True).eachpoint(lambda l: one_void.val().located(l))  # voids under
        h02 = h01.cut(all_voids)

        tweezer_voids = h00.faces(">Z").workplane(invert=True).rarray(s.unit_x, s.unit_y, s.nx, s.ny, center=True).eachpoint(lambda l: tweezer_void.val().located(l))  # tweezer voids
        h03 = h02.cut(tweezer_voids)

        #tweezer_voids2 = h00.faces(">Z").workplane(invert=True).rarray(s.unit_x, s.unit_y, s.nx, s.ny, center=True).eachpoint(lambda l: tweezer_void.rotate((0, 0, 0), (0, 0, 1), 90).val().located(l))  # tweezer voids
        #h04 = h03.cut(tweezer_voids2)
        h04 = h03

        h05 = h04.union(fatten_up.translate((0, 0, -s.support_plate_t)))

        if thread_void:
            thread_voids = h00.faces(">Z").workplane(invert=True).rarray(s.unit_x, s.unit_y, s.nx-1, s.ny-1, center=True).eachpoint(lambda l: thread_void.val().located(l))  # thread voids
            h06 = h05.cut(thread_voids)
        else:
            h06 = h05

        if cvx and cvy:
            center_void = CQ().box(cvx * s.unit_x, cvy * s.unit_y, z_len+s.support_plate_t, centered=(True, True, False)).translate((s.center_void_xshift,s.center_void_yshift,-s.support_plate_t))
            h07 = h06.union(center_void)  # ensure the big diameter cut fillets don't run into the normal geometry
            if thread_void:
                center_edge_threads = h00.faces(">Z").workplane(invert=True).rarray(s.unit_x, s.unit_y, cvx+1, cvy+1, center=True).eachpoint(lambda l: thread_void.val().located(l)).translate((s.center_void_xshift,s.center_void_yshift,0))
                h08 = h07.union(center_edge_threads)  # eliminate the threads on the edges of the sensor cutout
            else:
                h08 = h07
            h09 = h08.cut(center_void.edges("|Z").fillet(s.cut_tool_diameter_big / 2))
        else:
            h09 = h06

        h10 = h09.faces("<Z").chamfer(s.chamfer)

        # cut out the substrate corner pocket holes
        #pocket_corner_voids = CQ().workplane(offset=s.shelf_height).rarray(s.unit_x, s.unit_y, s.nx+1, s.ny+1, center=True).cylinder(height=s.wall_height, radius=s.cut_tool_diameter_big/2, centered=(True, True, False))
        pocket_corner_voids = CQ().workplane(offset=s.shelf_height).rarray(s.unit_x, s.unit_y, s.nx+1, s.ny+1, center=True).box(s.cut_tool_diameter_big, s.cut_tool_diameter_big, s.wall_height, centered=(True, True, False))
        h11 = h10.cut(pocket_corner_voids)
        #h11 = h10

        return h11
    
    def make_lid(self):
        s = self
        nx = 2
        ny = 2

        pusher_screw = CounterSunkScrew(size="M6-1", fastener_type="iso14581", length=20, simple=True)

        x_len = nx * s.unit_x - s.lid_fitment
        y_len = ny * s.unit_y - s.lid_fitment
        z_len = s.lid_thickness

        lid_hole = CQ().box(s.lid_hole[0], s.lid_hole[1], z_len, centered=(True, True, False)).edges("|Z").fillet(s.lid_hole_fillet)
        lid_hole_angles = CQ().workplane(offset=self.lid_inner_edge_height).sketch().rect(s.lid_hole[0], s.lid_hole[1]).vertices().fillet(s.lid_hole_fillet).finalize().extrude(s.lid_chamfer_thickness, taper=-1*(90-self.acceptance_angle))
        lid_hole = lid_hole.union(lid_hole_angles)

        wall_pocketx = CQ().sketch().slot(s.unit_x-s.cut_tool_diameter_big+s.lid_fitment, s.y_wall + s.lid_fitment, angle=00).finalize().extrude(s.wall_height)
        wall_pockety = CQ().sketch().slot(s.unit_y-s.cut_tool_diameter_big+s.lid_fitment, s.x_wall + s.lid_fitment, angle=90).finalize().extrude(s.wall_height)

        l00 = CQ().box(x_len, y_len, z_len, centered=(True, True, False))  # limits box

        hole_voids = l00.faces("<Z").workplane(invert=True).rarray(s.unit_x, s.unit_y, nx, ny, center=True).eachpoint(lambda l: lid_hole.val().located(l))
        l02 = l00.cut(hole_voids)

        wall_voids_x = CQ().rarray(s.unit_x, s.unit_y, nx, ny+1, center=True).eachpoint(lambda l: wall_pocketx.val().located(l))
        wall_voids_y = CQ().rarray(s.unit_x, s.unit_y, nx+1, ny, center=True).eachpoint(lambda l: wall_pockety.val().located(l))
        l03 = l02.cut(wall_voids_x)
        l04 = l03.cut(wall_voids_y)

        l05 = l04.faces(">Z").workplane().clearanceHole(pusher_screw, fit="Close")

        return l05.translate(((s.nx * s.unit_x)/2-s.unit_x,(s.ny * s.unit_y)/2-s.unit_y,s.shelf_height))


    def build(self, cvx=0, cvy=0):
        asy = cadquery.Assembly()

        # make the bottom piece
        thing = self.make_thing(cvx=cvx, cvy=cvy)
        bb = thing.findSolid().BoundingBox()
        self.lg.debug(f"extents = ({bb.xlen},{bb.ylen},{bb.zlen})")
        asy.add(thing, name="tray", color=cadquery.Color("gray"))

        lid = self.make_lid()
        asy.add(lid, name="lid", color=cadquery.Color("red"))

        # make substrates
        s_thickness = self.max_substrate_thickness
        substrate = CQ().box(self.x_nom, self.x_nom, s_thickness, centered=(True, True, False))
        substrates = CQ().rarray(self.unit_x, self.unit_y, self.nx, self.ny, center=True).eachpoint(lambda l: substrate.val().located(l)).translate((0,0,self.shelf_height))
        asy.add(substrates, name="substrates", color=cadquery.Color("blue"))

        return asy


def main():
    try:
        wrk_dir = pathlib.Path(__file__).parent
    except Exception as e:
        wrk_dir = pathlib.Path.cwd()

    if "show_object" in globals():  # we're in cq-editor

        def lshow_object(*args, **kwargs):
            return show_object(*args, **kwargs)

    else:
        lshow_object = None

    number_x = 11  # 12 for C02 laser
    number_y = 8  # 9 for CO2 laser
    t = LaserLabelHolder(number_x, number_y)
    center_void_x = 2  # space for sensor
    center_void_y = 2  # space for sensor
    asy = t.build(cvx=center_void_x, cvy=center_void_y)

    built = {"plate": {"assembly": asy}}
    twod_to_threed.TwoDToThreeD.outputter(built, wrk_dir=wrk_dir, save_steps=False, save_stls=False, show_object=lshow_object)


# temp is what we get when run via cq-editor
if __name__ in ["__main__", "temp"]:
    main()
