#!/usr/bin/env python3

import cadquery
from cadquery import CQ
import geometrics.toolbox.utilities as tbutil
from geometrics.toolbox import twod_to_threed
import pathlib
import logging


class QuartzAdapter(object):
    # units are mm
    x_nom = 30.0  # nominal substrate x dim
    y_nom = 30.0  # nominal substrate y dim

    ear_hole_spacing = 12
    ear_hole_diameter = 8
    ledge_depth = 1.5
    
    disk_diameter = 13
    disk_fitment = 0.2
    hole_diameter = 12
    chamfer_edge_spacing = 0.1

    hole_chamfer_length = (x_nom/2-hole_diameter)/2-chamfer_edge_spacing
    top_chamfer_length = 0.5

    total_thickness = ledge_depth + hole_chamfer_length + chamfer_edge_spacing

    fillet = 3


    def __init__(self):
        # setup logging
        self.lg = logging.getLogger(__name__)
        self.lg.setLevel(logging.DEBUG)

        ch = logging.StreamHandler()
        logFormat = logging.Formatter(("%(asctime)s|%(name)s|%(levelname)s|%(message)s"))
        ch.setFormatter(logFormat)
        self.lg.addHandler(ch)

    def make_thing(self):
        s = self

        a00 = CQ().box(s.x_nom, s.y_nom, s.total_thickness, centered=(True, True, False))

        #a01 = a00.faces(">Z").edges().chamfer(s.top_chamfer_length)
        a01 = a00

        a02 = a01.faces(">Z").workplane().rarray(s.x_nom/2, s.y_nom/2, 2, 2).circle(radius=(s.disk_diameter+s.disk_fitment)/2).cutBlind(-s.ledge_depth)

        a03 = a02.faces(">Z").workplane().rarray(s.x_nom/2, s.y_nom/2, 2, 2).circle(radius=(s.hole_diameter)/2).cutThruAll()

        a04 = a03.faces("<Z").edges("not %Line").chamfer(s.hole_chamfer_length)

        a05 = a04.edges("|Z").fillet(self.fillet)
        a06 = a05.faces(">Z").edges("<X").chamfer(s.top_chamfer_length)

        return a06


    def build(self):
        asy = cadquery.Assembly()

        # make the bottom piece
        thing = self.make_thing()
        asy.add(thing, name="tray", color=cadquery.Color("gray"))

        # make substrates
        #s_thickness = self.max_substrate_thickness
        #substrate = CQ().box(self.x_nom, self.x_nom, s_thickness, centered=(True, True, False))
        #substrates = CQ().rarray(self.unit_x, self.unit_y, self.nx, self.ny, center=True).eachpoint(lambda l: substrate.val().located(l)).translate((0,0,self.shelf_height))
        #asy.add(substrates, name="substrates", color=cadquery.Color("blue"))

        return asy


def main():
    try:
        wrk_dir = pathlib.Path(__file__).parent
    except Exception as e:
        wrk_dir = pathlib.Path.cwd()

    if "show_object" in globals():  # we're in cq-editor

        def lshow_object(*args, **kwargs):
            return show_object(*args, **kwargs)

    else:
        lshow_object = None

    t = QuartzAdapter()

    asy = t.build()

    built = {"plate": {"assembly": asy}}
    twod_to_threed.TwoDToThreeD.outputter(built, wrk_dir=wrk_dir, save_steps=False, save_stls=False, show_object=lshow_object)


# temp is what we get when run via cq-editor
if __name__ in ["__main__", "temp"]:
    main()
