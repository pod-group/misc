#!/usr/bin/env python

# computes various beta values for NTCs via stainhart-hart coefficents
# see https://en.wikipedia.org/wiki/Steinhart%E2%80%93Hart_equation

import math

# beta these are the betas we want to know
beta_ts = ((0,50),(25,50),(25,75),(25,85),(25,100))

# kelvin-c offset
koff = 273.15

# the temps and resistances we know
T = (25+koff, 0+koff, 85+koff)
R = (100*1000, (26.74*1000+27.83*1000)/2, 100*1000*math.exp(3435*((85+koff) - (25+koff))/((25+koff)*(85+koff))))

# compute Steinhart–Hart equation coefficents
L = (math.log(R[0]), math.log(R[1]), math.log(R[2]))
Y = (1/T[0], 1/T[1], 1/T[2])
gamma2 = (Y[1]-Y[0])/(L[1]-L[0])
gamma3 = (Y[2]-Y[0])/(L[2]-L[0])

C = ((gamma3-gamma2)/(L[2]-L[1]))/(L[0]+L[1]+L[2])
B = gamma2 - C*(L[0]**2+L[0]*L[1]+L[1]**2)
A = Y[0]-(B +L[0]**2*C)*L[0]

# the temperature point's we'll need
unique_ts = tuple(set([n for ns in beta_ts for n in ns]))

# compute the R values we're interested in
R_calc = {}
for temp in unique_ts:
    x = 1/C*(A-1/(temp+koff))
    y = math.sqrt((B/(3*C))**3+x**2/4)
    R_calc[temp] = math.exp((y-x/2)**(1/3)-(y+x/2)**(1/3))
    #print(f"R@{temp}C: {R_calc[temp]/1000} kOhm")

# compute the beta values we're interested in
for t1,t2 in beta_ts:
    beta = abs(math.log(R_calc[t1]/R_calc[t2])/(1/(t1+koff)-1/(t2+koff)))
    print(f"Beta[{t1},{t2}] = {beta:.0f}K")