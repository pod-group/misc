#!/usr/bin/env python3
import cadquery
from cadquery import CQ, cq
import geometrics.toolbox as tb
import logging
import pathlib

class OpticsTable(object):
  system_outname_base = "optics_table"
  dxf_filepath = pathlib.Path("wavelabs_optical_table.dxf")
  dxf_layernames = [
    "0",
    "Defpoints",
    "construction",
    "dims",
    "old_table",
    "post_cutouts",
    "posts",
    "support",
    "table_a_outline",
    "table_b_outline",
    "table_mount_holes",
    "window"
  ]
  # all units in mm
  thickness = 6.35
  
  def __init__(self):
    self.table_a_wires = self.get_wires("table_a_outline")
    self.table_b_wires = self.get_wires("table_b_outline")
    self.mount_holes = self.get_wires("table_mount_holes")

  def get_wires (self, layername):
    """returns the wires from the given dxf layer"""
    # list of of all layers in the dxf
    to_exclude = [k for k in self.dxf_layernames if layername != k]
    dxf_obj = cadquery.importers.importDXF(str(self.dxf_filepath), exclude=to_exclude)
    return(dxf_obj.wires())
  
  def make_left_table(self):
    table = CQ().add(self.table_a_wires).toPending().extrude(self.thickness)
    mount_holes = CQ().add(self.mount_holes).toPending().extrude(self.thickness)
    return(table.cut(mount_holes))

  def make_right_table(self):
    table = CQ().add(self.table_b_wires).toPending().extrude(self.thickness)
    mount_holes = CQ().add(self.mount_holes).toPending().extrude(self.thickness)
    return(table.cut(mount_holes))
  
  def build(self):
    s = self
    asy = cadquery.Assembly()

    left_table = s.make_left_table()
    asy.add(left_table, name="left_table", color=cadquery.Color("RED"))

    right_table = s.make_right_table()
    asy.add(right_table, name="right_table", color=cadquery.Color("BLUE"))

    return asy
  
def main():
  s = OpticsTable()
  asy = s.build()
  
  if "show_object" in globals():
    #show_object(asy)
    for key, val in asy.traverse():
      shapes = val.shapes
      if shapes != []:
        c = cq.Compound.makeCompound(shapes)
        odict = {}
        if val.color is not None:
          co = val.color.wrapped.GetRGB()
          rgb = (co.Red(), co.Green(), co.Blue())
          odict['color'] = rgb
        show_object(c.locate(val.loc), name=val.name, options=odict)

  elif __name__ == "__main__":

    # save system step
    asy.save(f"{s.system_outname_base}.step")

    # save system std
    cadquery.exporters.assembly.exportCAF(asy, f"{s.system_outname_base}.std")

    save_indivitual_stls = False
    save_indivitual_steps = True
    save_indivitual_breps = False

    if (save_indivitual_stls == True) or (save_indivitual_steps == True) or (save_indivitual_breps == True):
        # loop through individual pieces
        for key, val in asy.traverse():
            shapes = val.shapes
            if shapes != []:
                # make sure we're only taking one of whatever this is
                this = val.obj.val()
                if hasattr(this, '__iter__'):
                    one = next(val.obj.val().__iter__())
                else:
                    one = this

                # save as needed
                if save_indivitual_stls == True:
                    cadquery.exporters.export(one, f'{val.name}.stl')
                if save_indivitual_steps == True:
                    cadquery.exporters.export(one, f'{val.name}.step')
                if save_indivitual_breps == True:
                    cq.Shape.exportBrep(one, f'{val.name}.brep')

main()
