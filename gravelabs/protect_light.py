#!/usr/bin/env python3

from pymodbus.client import ModbusTcpClient

#import time
#print(time.ctime())

with ModbusTcpClient('192.168.178.73') as client:
	try:
		temp_rslt = client.read_input_registers(0)
		temp = temp_rslt.registers[0]/10
		print(f"Measured temperature: {temp} degrees C")
	except Exception as e:
			client.write_coil(0, False)
			print("Disabling light")
	
	try:
		hi_rslt = client.read_discrete_inputs(224)
		hi_alarm = hi_rslt.getBit(0)
		print(f"Over temperature alarm: {hi_alarm}")
	except Exception as e:
			client.write_coil(0, False)
			print("Disabling light")
	
	try:
		lo_rslt = client.read_discrete_inputs(256)
		lo_alarm = lo_rslt.getBit(0)
		print(f"Under temperature alarm: {lo_alarm}")
	except Exception as e:
			client.write_coil(0, False)
			print("Disabling light")
	
	try:
		if lo_alarm or hi_alarm:
			client.write_coil(0, False)
			print("Disabling light")
		else:
			client.write_coil(0, True)
			print("Enabling light")
	except Exception as e:
			client.write_coil(0, False)
			print("Disabling light")
