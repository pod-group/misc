#!/usr/bin/env python3

import cadquery
from cadquery import CQ
from cqkit import export_iges_file
import math

bb = {"centerOption": "CenterOfBoundBox"}
po = {"centerOption": "ProjectedOrigin"}

plate_t = 2.5
corner_r = 4

plate_nom_xy = 95

base_corners = (
    (-plate_nom_xy, -plate_nom_xy),
    (-plate_nom_xy, +plate_nom_xy),
    (+plate_nom_xy, +plate_nom_xy),
    (+plate_nom_xy, -plate_nom_xy - 10),
)
# needed to make closed polygon
poly_corners = base_corners + (base_corners[0],)

screen_print_alignment_points = (
    (+74, -45),
    (+45, -74),
    (+45, +74),
    (+74, +45),
    (+42.5, -45),
    (+42.25, 13.25),
    (+13.25, -74),
    (-45, -74),
    (-45, +74),
    (-74, -45),
    (-74, -20),
    (-74, +13.25),
    (-74, +45),
)

# for slots along x
wafer_points_x = (
    (-41.375, +77.286),
    (-15.375, +77.286),
    (-50, -79.375),
    (-6.75, -79.375),
    (+18.25, -79.375),
    (+50, -79.375),
    (-55, -84),
    (+55, -84),
)

wafer_points_y = (
    (-79.375, +29),
    (-79.375, 0),
    (-79.375, -33.187),
    (-84, 0),
    (+79.625, 0),
    (+84.25, 0),
)

short_offset = 5
plate_clamp_short_left = (
    (-85, +90),
    (+85, +90),
)
plate_clamp_short_right = (
    (-85, -90),
    (+85, -90),
)
long_offset = 10
pce1 = (-90, 0)
pce2 = (+90, 0)
pce3 = (0, -90)
pce4 = (0, +90)

bigr_pair = 2.25
smr_pair = 0.8

spah_r = 1.5
sspah_r = 0.5

# slots
# wafer_slot = (1, 2.2)  # for sketch mode
wafer_slot = (3.2, 2.2)  # for slot2D

wafer_slot_threadr = 0.8  # for M2 threads

dev_spacing = 30
n_dim_dev = 5

aptur = 27

clip_spacing = 87

feature_thickness = 0.2

big_shimxy = 153
shim_t = 0.05

do_tapering = True  # chamfer the feature cuts by 45 degrees
if do_tapering:
    taper = 45  # degrees

do_shims = False

fs_face = CQ().sketch().polygon(poly_corners).vertices().fillet(corner_r)
fs_plate: cadquery.Workplane = fs_face.finalize().extrude(plate_t)
fs_plate = fs_plate.edges("#Z").chamfer(0.4)

bs_plate = CQ(fs_plate.findSolid())

if do_shims:
    shim = CQ().workplane(offset=-shim_t).sketch().rect(big_shimxy, big_shimxy).rarray(dev_spacing, dev_spacing, n_dim_dev, n_dim_dev).rect(aptur, aptur, mode="s").finalize().extrude(shim_t * 2)
    shim = shim.faces(">Z").edges().fillet(shim_t)

    bs_plate = bs_plate.union(shim.findSolid().locate(bs_plate.faces(">Z").workplane().plane.location))
    fs_plate = fs_plate.union(shim.findSolid().locate(fs_plate.faces("<Z").workplane().plane.location))

# make aligment marks
if do_tapering:
    bs_plate = bs_plate.faces(">Z").workplane(**po, invert=True).pushPoints(screen_print_alignment_points).circle(sspah_r).cutThruAll(taper=taper)
else:
    bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints(screen_print_alignment_points).circle(spah_r).cutBlind(plate_t - feature_thickness)
    bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints(screen_print_alignment_points).circle(sspah_r).cutThruAll()


fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints(wafer_points_x).slot2D(*wafer_slot).cutThruAll()
fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints(wafer_points_y).slot2D(*wafer_slot, angle=90).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints(wafer_points_x + wafer_points_y).circle(wafer_slot_threadr).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(p[0] + short_offset, p[1]) for p in plate_clamp_short_left]).circle(smr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(p[0] + short_offset, p[1]) for p in plate_clamp_short_left]).circle(bigr_pair).cutThruAll()
fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(p[0] - short_offset, p[1]) for p in plate_clamp_short_left]).circle(bigr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(p[0] - short_offset, p[1]) for p in plate_clamp_short_left]).circle(smr_pair).cutThruAll()
fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(p[0] + short_offset, p[1]) for p in plate_clamp_short_right]).circle(bigr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(p[0] + short_offset, p[1]) for p in plate_clamp_short_right]).circle(smr_pair).cutThruAll()
fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(p[0] - short_offset, p[1]) for p in plate_clamp_short_right]).circle(smr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(p[0] - short_offset, p[1]) for p in plate_clamp_short_right]).circle(bigr_pair).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(pce1[0], pce1[1] + long_offset)]).circle(smr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(pce1[0], pce1[1] + long_offset)]).circle(bigr_pair).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(pce1[0], pce1[1] - long_offset)]).circle(bigr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(pce1[0], pce1[1] - long_offset)]).circle(smr_pair).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(pce2[0], pce2[1] + long_offset)]).circle(bigr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(pce2[0], pce2[1] + long_offset)]).circle(smr_pair).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(pce2[0], pce2[1] - long_offset)]).circle(smr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(pce2[0], pce2[1] - long_offset)]).circle(bigr_pair).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(pce3[0] + long_offset, pce3[1])]).circle(bigr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(pce3[0] + long_offset, pce3[1])]).circle(smr_pair).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(pce3[0] - long_offset, pce3[1])]).circle(smr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(pce3[0] - long_offset, pce3[1])]).circle(bigr_pair).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(pce4[0] + long_offset, pce4[1])]).circle(smr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(pce4[0] + long_offset, pce4[1])]).circle(bigr_pair).cutThruAll()

fs_plate = fs_plate.faces(">Z").workplane(**po).pushPoints([(pce4[0] - long_offset, pce4[1])]).circle(bigr_pair).cutThruAll()
bs_plate = bs_plate.faces("<Z").workplane(**po, invert=True).pushPoints([(pce4[0] - long_offset, pce4[1])]).circle(smr_pair).cutThruAll()


def clipCutter(loc: cadquery.Location) -> cadquery.Solid | cadquery.Compound:
    clip_spot_length = 10
    clip_flat_fillet = 0.5
    clip_flat_depth = 1
    clip_ramp_depth = 1
    clip_ramp_length = 7
    fab_fillet = clip_flat_depth*0.75
    clip_spot_width = 7 + 2*fab_fillet
    ramp_neg = CQ("ZY").workplane(**po, offset=-clip_spot_width / 2).moveTo(0, -clip_ramp_depth).lineTo(0, -clip_flat_depth - clip_ramp_depth).lineTo(-clip_ramp_length, -clip_flat_depth).close().extrude(clip_spot_width)

    ip_neg = CQ().workplane(**po, offset=-clip_spot_length, origin=(0, -clip_flat_depth)).box(clip_spot_width, clip_flat_depth, clip_spot_length, centered=(True, False, False)).edges("|Y and <Z").fillet(clip_flat_fillet).union(ramp_neg.findSolid())

    ip_neg = ip_neg.edges("not(<<Y[0] or <<Z[0])").fillet(fab_fillet)

    return ip_neg.findSolid().located(loc)


def clipCutter180(loc: cadquery.Location) -> cadquery.Solid | cadquery.Compound:
    """clip void rotated around Z by 180 deg"""
    clip_spot_length = 10
    clip_flat_fillet = 0.5
    clip_flat_depth = 1
    clip_ramp_depth = 1
    clip_ramp_length = 7
    fab_fillet = clip_flat_depth*0.75
    clip_spot_width = 7 + 2*fab_fillet
    ramp_neg = CQ("ZY").workplane(offset=-clip_spot_width / 2).moveTo(0, -clip_ramp_depth).lineTo(0, -clip_flat_depth - clip_ramp_depth).lineTo(-clip_ramp_length, -clip_flat_depth).close().extrude(clip_spot_width)

    ip_neg = CQ().workplane(offset=-clip_spot_length, origin=(0, -clip_flat_depth)).box(clip_spot_width, clip_flat_depth, clip_spot_length, centered=(True, False, False)).edges("|Y and <Z").fillet(clip_flat_fillet).union(ramp_neg.findSolid())

    ip_neg = ip_neg.edges("not(<<Y[0] or <<Z[0])").fillet(fab_fillet)

    return ip_neg.findSolid().rotate((0, 0, 0), (0, 0, 1), 180).located(loc)


fs_plate = fs_plate.faces(">X").workplane(**bb).center(0, +plate_t / 2).pushPoints([(0, clip_spacing / 2), (0, -clip_spacing / 2)]).cutEach(clipCutter)
bs_plate = bs_plate.faces(">X").workplane(**bb).center(0, -plate_t / 2).pushPoints([(0, clip_spacing / 2), (0, -clip_spacing / 2)]).cutEach(clipCutter180)

fs_plate = fs_plate.faces("<X").workplane(**bb).center(0, +plate_t / 2).pushPoints([(0, clip_spacing / 2), (0, -clip_spacing / 2)]).cutEach(clipCutter)
bs_plate = bs_plate.faces("<X").workplane(**bb).center(0, -plate_t / 2).pushPoints([(0, clip_spacing / 2), (0, -clip_spacing / 2)]).cutEach(clipCutter180)

if do_tapering:
    def featureCutter(loc: cadquery.Location) -> cadquery.Solid | cadquery.Compound:
        spt_neg: cadquery.Workplane = CQ()
        win1_neg: cadquery.Workplane = CQ().workplane(offset=-plate_t).center(0, 8.6611 / 2).sketch().rect(22.5, 5.6611).reset().vertices().fillet(0.75).finalize().extrude(plate_t*1/math.cos(math.radians(taper)), taper=-taper)
        #win2_neg: cadquery.Workplane = CQ().workplane(offset=-plate_t).center(0, -8.6611 / 2).sketch().rarray(8.0452, 1, 3, 1).rect(5.9095, 5.1611).reset().vertices().fillet(0.75).finalize().extrude(plate_t*1/math.cos(math.radians(taper)), taper=-taper)  # for 3+1
        win2_neg: cadquery.Workplane = CQ().workplane(offset=-plate_t).center(0, -8.6611 / 2).sketch().rect(22.5, 5.6611).reset().vertices().fillet(0.75).finalize().extrude(plate_t*1/math.cos(math.radians(taper)), taper=-taper)
        return spt_neg.union(win1_neg.findSolid()).union(win2_neg.findSolid()).findSolid().located(loc)
else:  # not 45 degree cuts
    def featureCutter(loc: cadquery.Location) -> cadquery.Solid | cadquery.Compound:
        spt_neg: cadquery.Workplane = CQ().workplane(offset=-plate_t + feature_thickness).sketch().rarray(1, 8.6611, 1, 2).rect(25, 8.1611).reset().vertices().fillet(2.25).finalize().extrude(plate_t - feature_thickness)
        win1_neg: cadquery.Workplane = CQ().workplane(offset=-plate_t).center(0, 8.6611 / 2).sketch().rect(22.5, 5.6611).reset().vertices().fillet(0.75).finalize().extrude(plate_t)
        #win2_neg: cadquery.Workplane = CQ().workplane(offset=-plate_t).center(0, -8.6611 / 2).sketch().rarray(8.0452, 1, 3, 1).rect(5.9095, 5.1611).reset().vertices().fillet(0.75).finalize().extrude(plate_t)  # for 3+1
        win2_neg: cadquery.Workplane = CQ().workplane(offset=-plate_t).center(0, -8.6611 / 2).sketch().rect(22.5, 5.6611).reset().vertices().fillet(0.75).finalize().extrude(plate_t)
        return spt_neg.union(win1_neg.findSolid()).union(win2_neg.findSolid()).findSolid().located(loc)


bs_plate = bs_plate.faces("<Z").workplane(**po, origin=(0, 0)).rarray(dev_spacing, dev_spacing, n_dim_dev, n_dim_dev).cutEach(featureCutter)
fs_plate = fs_plate.faces(">Z").workplane(**po, origin=(0, 0)).rarray(dev_spacing, dev_spacing, n_dim_dev, n_dim_dev).cutEach(featureCutter)


if "show_object" in locals():
    show_object(fs_plate, "front side frame")
    show_object(bs_plate, "back side frame")
else:
    cadquery.exporters.export(fs_plate, "front_side.step")
    cadquery.exporters.export(bs_plate, "back_side.step")
    export_iges_file(bs_plate, "back_side.igs", author="grey", organization="Oxford University")
    export_iges_file(fs_plate, "front_side.igs", author="grey", organization="Oxford University")
    bs_plate.findSolid().exportBrep("back_side.brep")
    fs_plate.findSolid().exportBrep("front_side.brep")
