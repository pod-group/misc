#!/usr/bin/env python3

import cadquery
from cadquery import CQ, cq
import geometrics.toolbox.utilities as tbutil
from geometrics.toolbox import twod_to_threed
from cq_warehouse.fastener import CounterSunkScrew
import cq_warehouse.extensions  # this does something even though it's not directly used
import pathlib
import logging
from typing import cast


class AgingTray(object):
    name = "aging_tray"
    # units are mm

    plate_t = 30
    hole_square = 30
    fill_r = 4
    cable_channel_w = hole_square - 2 * fill_r
    cable_channel_d = 15
    mount_spacing = 44
    pitch = [40, 58]

    hole_d = 20  # hole in underplate

    chamfer = 0.5

    def __init__(self):
        # setup logging
        self.lg = logging.getLogger(__name__)
        self.lg.setLevel(logging.DEBUG)

        ch = logging.StreamHandler()
        logFormat = logging.Formatter(("%(asctime)s|%(name)s|%(levelname)s|%(message)s"))
        ch.setFormatter(logFormat)
        self.lg.addHandler(ch)

    def make_thing(self, nx=6, ny=8):
        s = self
        co = "CenterOfBoundBox"
        no_threads = True

        #s.pitch[1] = s.pitch[1] - 14

        platexy = [s.pitch[0]*nx, s.pitch[1]*ny]

        plate = CQ().box(platexy[0], platexy[1], s.plate_t, centered=(True, True, False))

        bot_screw = CounterSunkScrew(size="M6-1", fastener_type="iso14581", length=s.plate_t, simple=no_threads)

        plate = plate.faces(">Z").workplane(origin=(0,0)).rarray(s.pitch[0], s.pitch[1], nx, ny).rect(self.hole_square, self.hole_square).cutThruAll()
        plate = cast(CQ, plate)

        plate = plate.edges("|Z").fillet(s.fill_r)
        plate = plate.faces("|Z").edges("%Line").edges("<Y").chamfer(s.chamfer)

        plate = plate.faces("<Z").workplane(origin=(0,0)).rarray(s.pitch[0], 1, nx, 1).rect(s.cable_channel_w, platexy[1]).cutBlind(-s.cable_channel_d)
        #plate = plate.faces("<Z").workplane(origin=(0,0)).rarray(1, s.pitch[1], 1, ny).rect(platexy[0], s.cable_channel_w).cutBlind(-s.cable_channel_d)

        plate = plate.faces("<Z").workplane(origin=(0,-s.mount_spacing/2),offset=-s.cable_channel_d).rarray(s.pitch[0], s.pitch[1], nx, ny).clearanceHole(bot_screw, fit="Close")
        plate = cast(CQ, plate)
        plate = plate.faces("<Z").workplane(origin=(0,+s.mount_spacing/2),offset=-s.cable_channel_d).rarray(s.pitch[0], s.pitch[1], nx, ny).clearanceHole(bot_screw, fit="Close")
        plate = cast(CQ, plate)

        return plate

    def build(self, nx=5, ny=5):
        asy = cadquery.Assembly()

        # make the bottom piece
        thing = self.make_thing(nx=nx, ny=ny)
        bb = thing.findSolid().BoundingBox()
        self.lg.debug(f"extents = ({bb.xlen},{bb.ylen},{bb.zlen})")
        asy.add(thing, name="tray", color=cadquery.Color("gray"))

        return asy


def main():
    try:
        wrk_dir = pathlib.Path(__file__).parent
    except Exception as e:
        wrk_dir = pathlib.Path.cwd()

    if "show_object" in globals():  # we're in cq-editor

        def lshow_object(*args, **kwargs):
            return show_object(*args, **kwargs)

    else:
        lshow_object = None

    t = AgingTray()
    number_x = 4
    number_y = 1
    asy = t.build(nx=number_x, ny=number_y)

    built = {"plate": {"assembly": asy}}
    twod_to_threed.TwoDToThreeD.outputter(built, wrk_dir=wrk_dir, save_steps=False, save_stls=False, show_object=lshow_object)


# temp is what we get when run via cq-editor
if __name__ in ["__main__", "temp"]:
    main()
