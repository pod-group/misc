// written by grey@christoforo.net

// no operation
#define NOP __asm__ __volatile__ ("nop\n\t")

// pin definitions
#define NOT_SLEEP_PIN 8
#define STEP_PIN 7
#define DIR_PIN 6

// step for this many ms for every keystroke we get
const unsigned long MS_PER_KEY = 20;

// freq for steps in Hz (31 is the min)
// edit this smaller for higher sensitivity
const unsigned int STEP_SPEED = 100;

// 20&100 is 2 pulses per press. keystrokes seem to come in at about 30ms period

//const int fwd_cmd = 0x57C8;  // key code for rotating forward  (up arrow key?)
//const int rev_cmd = 0x57D0;  // key code for rotating reverse (down arrow key?)

const int fwd_cmd = 0x66;  // key code for rotating forward
const int rev_cmd = 0x72;  // key code for rotating reverse

volatile int user_in = 0; // incoming serial data

volatile bool going_rev = true;  // keeps track of last direction

void setup() {  // runs once on boot
  // LED is on until initialization is done
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  
  // initialize the motor pins
  // pulled down by driver, set HIGH to exit sleep mode
  digitalWrite(NOT_SLEEP_PIN, LOW);

  // pulled down by driver, 1 pulse = 1/8 of a step = 1.8deg/8 = 0.225 degrees
  digitalWrite(STEP_PIN, LOW);

  // pulled down by driver (fwd is high, rev is low)
  digitalWrite(DIR_PIN, LOW);
  
  pinMode(NOT_SLEEP_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);

  // setup serial comms (via USB)
  Serial.begin(115200);
  while (!Serial) {
    NOP; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("\n\nSleep mode active. Waiting for first press...");

  // wait here (still in sleep mode) until we see the first valid command
  while ((user_in != fwd_cmd) && (user_in != rev_cmd)) {  // exit on fwd or rev commands
    user_in = Serial.read();
  }
  digitalWrite(NOT_SLEEP_PIN, HIGH);  // exit sleep
  Serial.println("Sleep mode disabled. Running normally.");

  digitalWrite(LED_BUILTIN, LOW);  // initialization done, LED off
}

//main loop
void loop() {
  user_in = Serial.read();

  if (user_in == rev_cmd){
    if (!going_rev){
      going_rev = true;
      digitalWrite(DIR_PIN, LOW);
    }
    tone(STEP_PIN, STEP_SPEED, MS_PER_KEY);
  } else if (user_in == fwd_cmd) {
    if (going_rev){
      going_rev = false;
      digitalWrite(DIR_PIN, HIGH);
    }
    tone(STEP_PIN, STEP_SPEED, MS_PER_KEY);
  }
}
