import pathlib
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from scipy.interpolate import interp1d
from scipy.integrate import simpson
from scipy.optimize import curve_fit, minimize, NonlinearConstraint
from scipy.constants import c, e, h

q = e

# define folders
cwd = pathlib.Path.cwd()
cal_meas_dark_folder = cwd.joinpath(r"WAVELABS_SPECTRAL_MISMATCH_DATA_2024-07-10").joinpath("cal_lamp_dark")
cal_meas_light_folder = cwd.joinpath(r"WAVELABS_SPECTRAL_MISMATCH_DATA_2024-07-10").joinpath("cal_lamp_light")
wavelabs_dark_folder = cwd.joinpath(r"WAVELABS_SPECTRAL_MISMATCH_DATA_2024-07-10").joinpath("wavelabs_dark")
wavelabs_light_folder = cwd.joinpath(r"WAVELABS_SPECTRAL_MISMATCH_DATA_2024-07-10").joinpath("wavelabs_light")

# gets lists of counts files
cal_meas_dark_files = [f for f in cal_meas_dark_folder.iterdir() if f.name.startswith("counts_")]
cal_meas_light_files = [f for f in cal_meas_light_folder.iterdir() if f.name.startswith("counts_")]
wavelabs_dark_files = [f for f in wavelabs_dark_folder.iterdir() if f.name.startswith("counts_")]
wavelabs_light_files = [f for f in wavelabs_light_folder.iterdir() if f.name.startswith("counts_")]

# get wavelegnths - all data uses same wavelengths data so just pick one                         
wls = np.genfromtxt(cal_meas_dark_folder.joinpath("wls_1720631509.9522784.csv"))

# get calibration lamp reference data
cal_ref = np.genfromtxt(r"reference_lamp.txt", delimiter="\t")

# read in and average counts files
cs_cal_meas_dark_data = np.zeros(len(wls))
for f in cal_meas_dark_files:
    cs_cal_meas_dark_data += np.genfromtxt(f)
cal_meas_dark_data = cs_cal_meas_dark_data / len(cal_meas_dark_files)

cs_cal_meas_light_data = np.zeros(len(wls))
for f in cal_meas_light_files:
    cs_cal_meas_light_data += np.genfromtxt(f)
cal_meas_light_data = cs_cal_meas_light_data / len(cal_meas_light_files)

cs_wavelabs_dark_data = np.zeros(len(wls))
for f in wavelabs_dark_files:
    cs_wavelabs_dark_data += np.genfromtxt(f)
wavelabs_dark_data = cs_wavelabs_dark_data / len(wavelabs_dark_files)

cs_wavelabs_light_data = np.zeros(len(wls))
for f in wavelabs_light_files:
    cs_wavelabs_light_data += np.genfromtxt(f)
wavelabs_light_data = cs_wavelabs_light_data / len(wavelabs_light_files)

# subtract backgrounds
cal_meas_b = cal_meas_light_data - cal_meas_dark_data
wavelabs_b = wavelabs_light_data - wavelabs_dark_data

# interpolate data
f_cal_meas = sp.interpolate.interp1d(wls, cal_meas_b, kind="cubic", bounds_error=False, fill_value=0)
f_cal_ref = sp.interpolate.interp1d(cal_ref[:,0], cal_ref[:,1], kind="cubic", bounds_error=False, fill_value=0)
f_wavelabs = sp.interpolate.interp1d(wls, wavelabs_b, kind="cubic", bounds_error=False, fill_value=0)

# choose interpolation wavelavelengths that map well onto spectral mismatch ranges
# interpolation will put all cal values to 0 below (350nm, i.e. cal range min)
# intensity values drop rapidly above ~1096nm due to spectrometer sensitivity, so cut just before this
wls_new = np.linspace(350, 1075, 726, endpoint=True)
cal_meas_i = f_cal_meas(wls_new)
cal_ref_i = f_cal_ref(wls_new)
wavelabs_i = f_wavelabs(wls_new)

# correct data with calibration data
corr = cal_ref_i / cal_meas_i
wavelabs_c = corr * wavelabs_i

# normalise data
wavelabs_n = wavelabs_c / np.max(wavelabs_c)

# get fraunhofer cell data
fraunhofer_meas_isc = 0.05296 # A, measured under wavelabs solar sim
fraunhofer_area = 0.02 * 0.02 # m2
fraunhofer_eqe = np.genfromtxt(r"EQE_044-2017_Fraunhofer_CREST.txt", delimiter="\t", skip_header=1)
f_fraunhofer_eqe = sp.interpolate.interp1d(fraunhofer_eqe[:,0], fraunhofer_eqe[:,1], kind="cubic", bounds_error=False, fill_value=0)

# get AM1.5G data
am15g = np.genfromtxt(r"AM1.5G.txt", delimiter="\t", skip_header=1)

# interpolate it
fraunhofer_eqe_i = f_fraunhofer_eqe(wls_new)

# get energies from wavelengths
es_new = h * c / (wls_new * 1e-9)

# calculate absolute spectrum correction factor
jsc_n = q * sp.integrate.simpson(y=fraunhofer_eqe_i * wavelabs_n / es_new, x=wls_new)
N_corr = (fraunhofer_meas_isc / fraunhofer_area) / jsc_n

# correct wavelabs spectrum to absolute units
wavelabs_abs = N_corr * wavelabs_n

# plot normalised spectrum
fig, ax = plt.subplots()
ax.plot(wls_new, wavelabs_n, label="wavelabs")
ax.set_ylim(0, 1)
ax.set_xlim(300, 1100)
ax.set_xlabel("wavelength (nm)")
ax.set_ylabel("normalised intensity (a.u.)")
fig.tight_layout()

# save figure and data
fig.savefig("2024-07-10_wavelabs_spectrum_norm.svg")
np.savetxt(
    "2024-07-10_wavelabs_spectrum_norm.txt",
    np.column_stack((wls_new, wavelabs_n)),
    fmt='%.6e',
    newline='\n',
    header="wavelength (nm)\tnormalised intensity (a.u.)",
    delimiter="\t",
    comments=''
)

# plot absolute spectrum compared to AM1.5G
fig, ax = plt.subplots()
ax.plot(am15g[:,0], am15g[:,1], label="AM1.5G")
ax.plot(wls_new, wavelabs_abs, label="wavelabs")
ax.set_ylim(0)
ax.set_xlim(300, 1100)
ax.set_xlabel("wavelength (nm)")
ax.set_ylabel("normalised intensity (a.u.)")
ax.legend(loc="best")
fig.tight_layout()

# save figure and data
fig.savefig("2024-07-10_wavelabs_spectrum_abs.svg")
np.savetxt(
    "2024-07-10_wavelabs_spectrum_abs.txt",
    np.column_stack((wls_new, wavelabs_abs)),
    fmt='%.6e',
    newline='\n',
    header="wavelength (nm)\tspectral irradiance (W*m-2*nm-1)",
    delimiter="\t",
    comments=''
)

