#!/usr/bin/env python3

import math
from cadquery import CQ, exporters

angle = 30
xyextent = 100
thickness = 30
fillet = 7
cross1 = 20
cross2 = 60

neg = CQ().sketch().rect(cross1,cross2).rect(cross2,cross1).clean().vertices().fillet(7).finalize().extrude(thickness*1/math.cos(math.radians(angle)), taper=-angle)
part = CQ().box(xyextent, xyextent, thickness, centered=(True, True, False)).cut(neg)

exporters.export(part, "wire_edm_cam_test.step")
