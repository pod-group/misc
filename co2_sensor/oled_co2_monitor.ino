// A simple CO2 meter using the Adafruit SCD30 breakout and the Adafruit 128x32 OLEDs
#include <Adafruit_SCD30.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

Adafruit_SCD30  scd30;
Adafruit_SH1107 display = Adafruit_SH1107(64, 128, &Wire);

void setup(void) {
  scd30.begin();

  display.begin(0x3C, true);
  scd30.setMeasurementInterval(2);
  
  display.display();
  delay(500);

  display.setTextSize(2);
  display.setTextColor(SH110X_WHITE);
  display.setRotation(1);
}


void loop() {
  if (scd30.dataReady()) {
    display.clearDisplay();
    display.setCursor(0,0);

    if (!scd30.read()){
      display.println("READ ERR");
      display.display();
      return;
    }
    
    display.setTextSize(1);
    display.print("CO2:");
    display.setTextSize(2);
    display.print(scd30.CO2, 2);
    display.setTextSize(1);
    display.println("[ppm]");

    display.setCursor(0, 20);
    display.setTextSize(1);
    display.print("TEMP:");
    display.setTextSize(2);
    display.print(scd30.temperature, 2);
    display.setTextSize(1);
    display.println("[degC]");

    display.setCursor(0, 40);
    display.setTextSize(1);
    display.print("RH:");
    display.setTextSize(2);
    display.print(scd30.relative_humidity, 2);
    display.setTextSize(1);
    display.println("[%]");

    display.display();    
  }

  delay(100);
}